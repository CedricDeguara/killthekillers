﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    float mousePositionInBlocks;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        mousePositionInBlocks = Input.mousePosition.x / Screen.width * 12f;

        Vector3 paddlePosition = new Vector3(Mathf.Clamp(mousePositionInBlocks - 7f, -5.5f, 5.5f), this.transform.position.y, 0f);

        this.transform.position = paddlePosition;
    }
}