﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    float mousePositionInBlocks;
    [SerializeField] GameObject arrowPrefab;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Fire();

        mousePositionInBlocks = Input.mousePosition.x / Screen.width * 12f;

        Vector3 paddlePosition = new Vector3(Mathf.Clamp(mousePositionInBlocks - 7f, -5.5f, 5.5f), this.transform.position.y, 0f);

        this.transform.position = paddlePosition;   
    }

    private void Fire()
    {
        //if a button linked to Fire1 is pressed
        if (Input.GetButtonDown("Fire1"))
        {

            GameObject arrow = Instantiate(arrowPrefab, this.transform.position, Quaternion.identity) as GameObject;
            arrow.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 15f);
        }
    }

}